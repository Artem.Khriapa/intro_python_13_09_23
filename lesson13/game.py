from abc import ABC, abstractmethod
from random import choice


class BaseAbstractPlayer(ABC):
    name = 'Unknown'

    @abstractmethod
    def get_figure(self, *args, **kwargs):
        pass


class HumanPlayer(BaseAbstractPlayer):
    name = 'Human'

    def get_figure(self, options: list):
        names = [obj.name for obj in options]
        while True:
            user_input = input(f'Enter one of {names}: ')
            if user_input not in names:
                print('Wrong input')
                continue

            for obj in options:
                if obj.name == user_input:
                    return obj


class AIPlayer(BaseAbstractPlayer):
    name = 'AI'

    def get_figure(self, options: dict):
        ai_choice = choice(options)
        print(f'HINT {ai_choice.name}')

        return ai_choice


class BaseAbstractGameFigure(ABC):

    def __eq__(self, other):
        if not isinstance(other, BaseAbstractGameFigure):
            raise TypeError

        return type(self) == type(other)

    @abstractmethod
    def __gt__(self, other):
        pass


class Rock(BaseAbstractGameFigure):
    name = 'Rock'

    def __gt__(self, other):
        if not isinstance(other, BaseAbstractGameFigure):
            raise TypeError
        if type(other) == Scissors:
            return True
        else:
            return False


class Scissors(BaseAbstractGameFigure):
    name = 'Scissors'

    def __gt__(self, other):
        if not isinstance(other, BaseAbstractGameFigure):
            raise TypeError
        if type(other) == Paper:
            return True
        else:
            return False


class Paper(BaseAbstractGameFigure):

    name = 'Paper'

    def __gt__(self, other):
        if not isinstance(other, BaseAbstractGameFigure):
            raise TypeError
        if type(other) == Rock:
            return True
        else:
            return False


class RSPGame:
    game_name = 'Rock Scissors Paper'
    player1 = None
    player2 = None
    rules = [Scissors(), Paper(), Rock()]

    def __init__(self, player1, player2):
        self.player1 = player1
        self.player2 = player2

    def play(self):
        print(f'{self.game_name} started for 1 time play')
        self._play()

    def _play(self):
        f1 = self.player1.get_figure(self.rules)
        f2 = self.player2.get_figure(self.rules)

        if f1 == f2:
            print('Draw')
        elif f1 > f2:
            print('player1 win with', f1.name)
        else:
            print('player2 win with', f2.name)

    def play_3_times(self):
        print(f'{self.game_name} started for 3 time play')
        for _ in range(3):
            self._play()


ai_player1 = AIPlayer()
human_player1 = HumanPlayer()

game = RSPGame(ai_player1, human_player1)

game.play()


