# computable attributes
#
# class Point:
#     _x = None
#     _y = None
#
#     def __init__(self, x_coord, y_coord):
#         self.x = x_coord
#         self.y = y_coord
#
#     def __str__(self):
#         return f'Point [{self.x}:{self.y}]'
#
#     def x_getter(self):
#         return self._x
#
#     def x_setter(self, val):
#         if not isinstance(val, (int, float)):
#             raise TypeError
#
#         self._x = val
#
#     x = property(x_getter, x_setter)
#
#     # y = property()
#     #
#     # @y.getter
#     # def y(self):
#     #     return self._y
#     #
#     # @y.setter
#     # def y(self, val):
#     #     if not isinstance(val, (int, float)):
#     #         raise TypeError
#     #     self._y = val
#
#     @property
#     def y(self):  # getter
#         return self._y
#
#     @y.setter
#     def y(self, val):
#         if not isinstance(val, (int, float)):
#             raise TypeError
#         self._y = val
#
#
# class Line:
#     _p1 = None
#     _p2 = None
#
#     def __init__(self, begin, end):
#         self.p1 = begin
#         self.p2 = end
#
#     def get_length(self):
#         c1 = (self.p1.x - self.p2.x) ** 2
#         c2 = (self.p1.y - self.p2.y) ** 2
#         return (c1 + c2) ** 0.5
#
#     # def length_getter(self):
#     #     print('---> length_getter')
#     #     c1 = (self.p1.x - self.p2.x) ** 2
#     #     c2 = (self.p1.y - self.p2.y) ** 2
#     #     return (c1 + c2) ** 0.5
#     #
#     # length = property(length_getter)
#
#     @property
#     def p1(self):
#         return self._p1
#
#     @p1.setter
#     def p1(self, value):
#         if not isinstance(value, Point):
#             raise TypeError
#         self._p1 = value
#
#     @property
#     def p2(self):
#         return self._p2
#
#     @p2.setter
#     def p2(self, value):
#         if not isinstance(value, Point):
#             raise TypeError
#         self._p2 = value
#
#     @property
#     def length(self):
#         side1 = (self.p1.x - self.p2.x) ** 2
#         side2 = (self.p1.y - self.p2.y) ** 2
#         return (side1 + side2) ** 0.5
#
#
# p1 = Point(0, 3)
# p2 = Point(4, 0)
#
# line1 = Line(p1, p2)
#
# # print(line1.p1)
# line1.p1 = Point(40, 10)
# line1.p2 = Point(20, 10)
#
# # print(line1.get_length())
# # print(line1.length)
# line1.p2.y = 100


# descriptor


# class OnlyInfFloatDescriptor:
#     _name = None
#
#     def __init__(self, name):
#         self._name = name
#
#     def __get__(self, instance, owner):
#         return instance.__dict__[self._name]
#
#     def __set__(self, instance, value):
#         if not isinstance(value, (int, float)):
#             raise TypeError
#         instance.__dict__[self._name] = value
#
#
# class Point:
#     x = OnlyInfFloatDescriptor('x')
#     y = OnlyInfFloatDescriptor('y')
#
#     def __init__(self, x_coord, y_coord):
#         self.x = x_coord
#         self.y = y_coord
#
#     def __str__(self):
#         return f'Point [{self.x}:{self.y}]'
#
#
# class Line:
#     _p1 = None
#     _p2 = None
#
#     def __init__(self, begin, end):
#         self.p1 = begin
#         self.p2 = end
#
#     def get_length(self):
#         c1 = (self.p1.x - self.p2.x) ** 2
#         c2 = (self.p1.y - self.p2.y) ** 2
#         return (c1 + c2) ** 0.5
#
#     # def length_getter(self):
#     #     print('---> length_getter')
#     #     c1 = (self.p1.x - self.p2.x) ** 2
#     #     c2 = (self.p1.y - self.p2.y) ** 2
#     #     return (c1 + c2) ** 0.5
#     #
#     # length = property(length_getter)
#
#     @property
#     def p1(self):
#         return self._p1
#
#     @p1.setter
#     def p1(self, value):
#         if not isinstance(value, Point):
#             raise TypeError
#         self._p1 = value
#
#     @property
#     def p2(self):
#         return self._p2
#
#     @p2.setter
#     def p2(self, value):
#         if not isinstance(value, Point):
#             raise TypeError
#         self._p2 = value
#
#     @property
#     def length(self):
#         side1 = (self.p1.x - self.p2.x) ** 2
#         side2 = (self.p1.y - self.p2.y) ** 2
#         return (side1 + side2) ** 0.5
#
#
# p1 = Point(0, 3)
# p2 = Point(4, 0)
#
#
# p1.x = 10
#
# print(p1.x)


class Example:
    attr1 = 'value in class'

    def __new__(cls, *args, **kwargs):
        instance = super().__new__(cls)
        return instance

    def __init__(self, val):
        self.attr1 = val

    def get_attr_from_obj(self):
        return self.attr1

    def get_attr_from_cls(self):
        return self.__class__.attr1

    @classmethod
    def get_from_cls(cls):
        return cls.attr1

    @staticmethod
    def get_from_static():
        return 'static'


ex = Example('value in object')
ex1 = Example('value in object1')

print(Example.attr1)

print(ex.get_attr_from_obj())
print(ex.get_attr_from_cls())
print(ex.get_from_cls())
print(ex1.get_attr_from_obj())
print(ex1.get_from_cls())
print(ex1.get_from_static())

