# dict
#
# my_dict = {'a': 'some value', 1: 12345}
#
# print(my_dict['a'])


# my_dict = {
#     False: '1234567',
#     1: 12345,
#     2.0: 12345.0,
#     'a': 'some value',
#     None: [1, 2, 3, 4],
#     (1, 2, 3): {1: 2, '2': 3},
#     'login': 'abcde',
#     'password': 'abcde',
# }
#
# print(my_dict)
# print(my_dict[1])
# print(my_dict[(1, 2, 3)]['2'])
# print(my_dict[2.0])
# print(my_dict[None][2])
#
#
# print(my_dict[1])
#
# my_dict[1] = 'asdfghjk'
# print(my_dict)
#
# my_dict[123] = [1, 2, 3]
# print(my_dict)


# my_dict = {
#     False: '1234567',
#     1: 12345,
#     2.0: 12345.0,
#     'a': 'some value',
#     None: [1, 2, 3, 4],
#     (1, 2, 3): {1: 2, '2': 3},
#     'login': 'abcde',
#     'password': 'abcde',
#     # '12': [1, 2, 3],
# }
#
# print(my_dict.get('12'))
# print(my_dict.get('login'))
# print(my_dict.get('12', 'some value'))
#
# dct1 = {1: 2}
# dct1.update({1: 4})
#
# print(dct1)


# my_dict = {
#     False: '1234567',
#     1: 12345,
#     2.0: 12345.0,
#     'a': 'some value',
#     None: [1, 2, 3, 4],
#     (1, 2, 3): {1: 2, '2': 3},
#     'login': 'abcde',
#     'password': 'abcde',
#     # '12': [1, 2, 3],
#     'new key': 'old value'
# }

# for key in my_dict:
#     print(key, my_dict[key])
#
# print(1 in my_dict)


# for key in my_dict.keys():
#     print(key, my_dict[key])

#
# for value in my_dict.values():
#     print(value)

#
# for item in my_dict.items():
#     print(item)
#     # key = item[0]
#     # value = item[1]
#     key, value = item
#
# for key, value in my_dict.items():
#     print(key, value)
#
#
# my_dict[1234] = 12345
# print(my_dict)
# my_dict.pop(1234)
# print(my_dict)
#
# res = my_dict.setdefault('new key', 'new value')
#
# print(res)
# print(my_dict)


# lst1 = [1, 2, 3]
# lst2 = lst1
#
# print(id(lst1), lst1)
# print(id(lst2), lst2)
#
# lst2.append(4)
#
# print(lst1)
# print(lst2)


# lst1 = [1, 2, 3]
# # lst2 = lst1.copy()
# lst2 = lst1[:]
#
# print(id(lst1), lst1)
# print(id(lst2), lst2)
#
# lst2.append(4)
#
# print(lst1)
# print(lst2)


# lst1 = [1, 2, 3, ['a, b']]
# lst2 = lst1.copy()
#
# print(id(lst1), lst1)
# print(id(lst2), lst2)
#
# lst2.append(4)
# lst2[3].append('c')
#
# print(id(lst1[3]))
# print(id(lst2[3]))

# import copy
#
# lst1 = [1, 2, 3, ['a, b']]
# lst2 = copy.deepcopy(lst1)
#
# print(id(lst1), lst1)
# print(id(lst2), lst2)
#
# lst2.append(4)
# lst2[3].append('c')
#
# print(id(lst1[3]))
# print(id(lst2[3]))
#
# print(lst1)
# print(lst2)


# set / frozenset

# my_set = {1, 2.2, False, 1, 'QWER', (1, 2, 3,)}
#
# print(my_set)
# lst = [1, 2, 3, 4, 51, 1, 2, 3, 4]
# my_set = set(lst)
# print(my_set)

# my_set = {1, 2.2, False, 1, 'QWER', (1, 2, 3,)}
#
# print(my_set)
#
# my_set.add(3)
# print(my_set)
# print(1 in my_set)
# print(my_set.pop())
# print(my_set)
# my_set.remove(1)
# print(my_set)
#
# print({1, 2, 3} == {3, 2, 1})

# my_set = {1, 2.2, False, 1, 'QWER', (1, 2, 3,)}
#
# for i in my_set:
#     print(i)
#
#
# my_fset = frozenset({1, 2.2, False, 1, 'QWER', (1, 2, 3,)})
# for i in my_fset:
#     print(i)

# my_dict = {
#     1: 1,
#     2: 2,
# }
#
# print(my_dict)
#
# my_dict = dict(a=10, b=20)
# print(my_dict)
#
# my_dict = dict([('a', 10), ('b', 20)])
# print(my_dict)

#
# my_dict = {}
#
# for numb in range(5):
#     my_dict[numb] = numb ** 2
#
# print(my_dict)
#
#
# my_dict = {
#     'outer key': {'inner key': 3},
# }
#
# my_dict['outer key'][0] = 123
# print(my_dict)
# print(my_dict['outer key']['inner key'])


# ternary operator

# bool_data = True
#
# if bool_data:
#     vote = 'Yes'
# else:
#     vote = 'No'
#
# print(vote)
#
#
# vote = 'Yes' if bool_data else 'No'
# print(vote)
#
# vote = [] if bool_data else {}
# print(vote)


# comprehensions


# lst = [14, 22, 3, 42, 5, 69, 74, 80, 0]
#
# lst2 = []
# for number in lst:
#     if number % 2 == 0:
#         if number < 25:
#             lst2.append(-number)
#         else:
#             lst2.append(number)
# print(lst2)
#
# lst = [str(numb ** 2) for numb in range(10)]
# lst2 = [-numb if numb < 25 else numb for numb in lst if numb % 2 == 0]
# print(lst2)


# lst2 = [str(numb ** 2) for numb in range(10)]
# print(lst2)
#
#
# my_set = {str(numb ** 2) for numb in range(10)}
# print(my_set)
#
#
# my_dict = {numb: str(numb ** 2) for numb in range(10)}
# print(my_dict)
#
#
# result = (str(numb ** 2) for numb in range(10))
# print(result)


# lst = [numb ** 2 for numb in range(10**100)]


gen = (numb ** 2 for numb in range(10**100))

print(gen)

for i in gen:
    print(i)

