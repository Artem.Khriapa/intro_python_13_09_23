# Rock Scissors Paper

# game
    # get AI choice
    # get Player choice
    # compare
#
# rules = {
#     'Rock': 'Scissors',
#     'Scissors': 'Paper',
#     'Paper': 'Rock',
# }
#
# f1 = 'Rock'
# f2 = 'Paper'
#
#
# if f1 == f2:
#     print('Draw')
# elif rules[f1] == f2:
#     print(f1)
# else:
#     print(f2)


# class Example:
#     attr1 = 'asdfg'
#     attr2 = 1234
#
#     # def __repr__(self):
#     #     return f'Example obj with {self.attr1} {self.attr2}'
#
#     def __str__(self):
#         return f'Example {self.attr1} {self.attr2}'
#
#     def __int__(self):
#         return self.attr2
#
#
# ex = Example()
# print(ex)
#
# str_obj = str(ex)
# print(str_obj)
#
# int_obj = int(ex)
# print(int_obj)

#
# class Example:
#     attr1 = 'asdfg'
#     attr2 = 1234
#
#     def __getattribute__(self, item):
#         print('__getattribute__', item)
#         try:
#             return object.__getattribute__(object, item)
#         except:
#             print('Not found', item)
#
#     def __setattr__(self, key, value):
#         print('__setattr__', key, value)
#         object.__setattr__(self, key, value)
#
#
# ex = Example()
#
# print(ex.attr1sdf)
# print(getattr(ex, 'attr1sdf'))
#
# ex.hagvscgyv = 12345

#
# class Example:
#     attr1 = 'asdfg'
#     attr2 = 1234
#
#     def __getitem__(self, item):
#         print('__getitem__', item)
#         try:
#             return object.__getattribute__(self, item)
#         except:
#             print('Not found', item)
#
#     def __setitem__(self, key, value):
#         print('__setitem__', key, value)
#         object.__setattr__(self, key, value)
#
#
# ex = Example()
#
# print(ex['attr1'])
#
# ex['attr1'] = 12345
# print(ex['attr1'])
# print(ex[1:2])


class Example:
    attr1 = 'asdfg'
    attr2 = 1234

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            raise TypeError
        return self.attr2 == other.attr2

    def __ne__(self, other):
        if not isinstance(other, self.__class__):
            raise TypeError
        return self.attr2 != other.attr2

    def __lt__(self, other):
        if not isinstance(other, self.__class__):
            raise TypeError
        return self.attr2 < other.attr2

    def __gt__(self, other):
        if not isinstance(other, self.__class__):
            raise TypeError
        return self.attr2 > other.attr2

    def __ge__(self, other):
        if not isinstance(other, self.__class__):
            raise TypeError
        return self.attr2 >= other.attr2

    def __le__(self, other):
        if not isinstance(other, self.__class__):
            raise TypeError
        return self.attr2 <= other.attr2


ex1 = Example()
ex1.attr2 = 10
ex2 = Example()
ex2.attr2 = 10

print(ex1 == ex2)
print(ex1 != ex2)
print(ex1 > ex2)
print(ex1 < ex2)
print(ex1 >= ex2)
print(ex1 <= ex2)
