# recursion
# decorator

# def fact_rec(n):
#     print(f'fact_rec {n}')
#
#     if n == 1:
#         return 1
#
#     return fact_rec(n - 1)
#
# res = fact_rec(5)


# def fact_plain(n):
#     res = 1
#     for i in range(1, n+1):
#         res *= i
#
#     return res
#
#
# res = fact_plain(5)
# print(res)


# 1 1 2 3 5 8 13 21

# def fib_rec(n):
#
#     if n <= 2:
#         return 1
#
#     return fib_rec(n - 1) + fib_rec(n - 2)
#
#
# res = fib_rec(40)
#
# print(res)

# counter = 0
#
#
# def fib_rec(n):
#     global counter
#     counter += 1
#
#     if n <= 2:
#         return 1
#     return fib_rec(n - 1) + fib_rec(n - 2)
#
#
# # res = fib_rec(40)
# # print('counter: ', counter)
# # print(res)
#
#
# def fib_plain(n):
#     fib1 = fib2 = 1
#
#     if n <= 2:
#         return fib2
#
#     for i in range(n-2):
#         fib1, fib2 = fib2, fib1 + fib2
#
#     return fib2
#
#
# print(fib_plain(1000))


# data = {
#     '0-1': '0a',
#     '0-2': {
#         '1-1': '1a',
#         '1-2': '2a',
#         '1-3': {
#             '2-1': '2a',
#             '2-2': {
#                 '3-1': []
#             }
#         },
#     },
#     '0-3': '0b'
# }
#
#
# # for k0, v0 in data.items():
# #     print(k0, v0)
# #     if isinstance(v0, dict):
# #         for k1, v1 in v0.items():
# #             print(k1, v1)
# #             if isinstance(v1, dict):
# #                 for k2, v2 in v1.items():
# #                     print(k2, v2)
#
#
# def deep_search(data_dict):
#     for k, v in data_dict.items():
#         if isinstance(v, dict):
#             print('key', k)
#             return deep_search(v)
#         print('key', k, 'value', v)
#
#
# deep_search(data)


# decorator


# def foo():
#     print('in function foo')
#
#
# def bar(func):
#     print('in function bar')
#     func()
#
#     def baz():
#         print('in function baz')
#
#     return baz
#
#
# result = bar(foo)
#
# print(type(result))
#
# result()

#
# def my_function():
#     print('I\'m function my_function')
#
#
# def my_function2(a, b):
#     print('I\'m function my_function2')
#
#
# def my_decorator(func):
#
#     def _wrapper(*args, **kwargs):
#         print('in my_decorator before function')
#         res = func(*args, **kwargs)
#         print('in my_decorator after function')
#         return res
#
#     print(id(_wrapper))
#     return _wrapper
#
#
# my_function = my_decorator(my_function)
# print(id(my_function))
# my_function()


# def my_decorator(func):
#
#     def _wrapper(*args, **kwargs):
#         print('in my_decorator before function')
#         res = func(*args, **kwargs)
#         print('in my_decorator after function')
#         return res
#
#     return _wrapper
#
#
# def my_decorator2(func):
#
#     def _wrapper(*args, **kwargs):
#         print('in my_decorator2 before function')
#         res = func(*args, **kwargs)
#         print('in my_decorator2 after function')
#         return res
#
#     return _wrapper
#
#
# @my_decorator  # my_function = my_decorator(my_function)
# @my_decorator2  # my_function = my_decorator2(my_function)
# def my_function():
#     print('I\'m function my_function')
#
#
# my_function()


# def my_decorator(func):
#
#     def _wrapper(*args, **kwargs):
#         print('in my_decorator before function')
#         res = func(*args, **kwargs)
#         print('in my_decorator after function')
#         return res
#
#     return _wrapper
#
#
# @my_decorator  # my_function = my_decorator2(my_function)
# def my_function():
#     print('I\'m function my_function')
#
#
# @my_decorator
# def my_function2():
#     print('I\'m function my_function2')
#
#
# my_function()
# my_function2()


# def check_args_int(func):
#     def _wrapper(*args, **kwargs):
#         print('check_args_int')
#         for arg in args:
#             if type(arg) != int:
#                 raise TypeError
#         for val in kwargs.values():
#             if type(val) != int:
#                 raise TypeError
#
#         res = func(*args, **kwargs)
#         return res
#
#     return _wrapper
#
#
# @check_args_int
# def foo(a, b, c):
#     return (a + b) ** c
#
#
# foo(1, 2.2, 3)

# def check_args(*types):
#     def _check_args(func):
#         def _wrapper(*args, **kwargs):
#             for arg in args:
#                 if not isinstance(arg, types):
#                     raise TypeError
#             for val in kwargs.values():
#                 if not isinstance(val, types):
#                     raise TypeError
#             res = func(*args, **kwargs)
#             return res
#         return _wrapper
#     return _check_args
#
#
# @check_args(int, float)  # foo = check_args(int, float)(foo)
# def foo(a, b, c):
#     return (a + b) ** c


# foo(1, 2.2, '3')

#
# def safe_run(func):
#     def _wrapper(*args, **kwargs):
#         try:
#             return func(*args, **kwargs)
#         except Exception as e:
#             print(e)
#         return
#     return _wrapper
#
#
# @safe_run  # foo = safe_run(foo)
# def foo(a, b, c):
#     return (a + b) ** c
#
#
# foo('1', 2, [])


# def args_to_string(func):
#     def _wrapper(*args, **kwargs):
#         _new_args = []
#         for arg in args:
#             _new_args.append(str(arg))
#
#         for key, val in kwargs.items():
#             kwargs[key] = str(val)
#
#         res = func(*_new_args, **kwargs)
#         return res
#
#     return _wrapper
#
#
# @args_to_string
# def foo(a, b, c):
#     print(type(a), type(b), type(c))
#     return a + b + c
#
#
# print(foo(1, 1.2, [1, 2]))

import time
time.time()


def my_decorator(func):

    def _wrapper(*args, **kwargs):
        print('in my_decorator before function')
        res = func(*args, **kwargs)
        print('in my_decorator after function')
        return res

    return _wrapper


def my_decorator2(func):

    def _wrapper(*args, **kwargs):
        print('in my_decorator2 before function')
        res = func(*args, **kwargs)
        print('in my_decorator2 after function')
        return res

    return _wrapper


@my_decorator  # my_function = my_decorator(my_decorator2(my_function)))
@my_decorator2
def my_function():
    print('I\'m function my_function')


my_function()
