# functions

# def my_function(arg1, arg2):
#     res = (arg1 + arg2) ** 2
#     return res
#
#
# my_res = my_function(1, 2)
# print(my_res)
#
# my_res = my_function(3, 2)
# print(my_res)


# def math_processing(arg1, arg2):
#     print(f'arg1 is {arg1}')
#     print(f'arg2 is {arg2}')
#     res = (arg1 + arg2) ** 2
#     return res


# my_res = math_processing(10, 20)  # positional
# print(my_res)
#
# my_res = math_processing(20, 10)  # positional
# print(my_res)

# my_res = math_processing(arg1=20, arg2=10)  # named
# print(my_res)
#
# my_res = math_processing(arg2=20, arg1=10)  # named
# print(my_res)
#
#
# def math_processing(arg1, arg2, arg3):
#     print(f'arg1 is {arg1}, {type(arg1)}')
#     print(f'arg2 is {arg2}, {type(arg2)}')
#     print(f'arg3 is {arg3}, {type(arg3)}')
#     res = (arg1 + arg2) ** arg3
#     return res
#
#
# my_res = math_processing(20, 10, 2)
# print(my_res)
#
# my_res = math_processing(arg3=20, arg2=10, arg1=2)
# print(my_res)
#
# my_res = math_processing(10, arg3=2, arg2=10)
# print(my_res)


# def get_int(prompt):
#     while True:
#         try:
#             res = int(input(f'{prompt} (int number): '))
#             return res
#         except Exception:
#             print('Wrong input!')
#
#
# # number = get_int('Enter your age')
# # print('number is', number)
#
#
# user_prompt = 'Enter year of birth'
# number = get_int(user_prompt)
# print('number is', number)

#
# def get_int(prompt, lower_limit, upper_limit):
#     while True:
#         try:
#             res = int(input(f'{prompt} (int number): '))
#         except Exception:
#             print('Wrong input!')
#         else:
#             if res < lower_limit or res > upper_limit:
#                 print(f'Number should be between {lower_limit} and {upper_limit}!')
#                 continue
#             else:
#                 return res
#
#
# user_prompt = 'Enter year of birth'
# number = get_int(user_prompt, 1900, 2023)
# print('number is', number)


# def get_int(prompt='Enter number', lower_limit=0, upper_limit=150):
#     while True:
#         try:
#             res = int(input(f'{prompt} (int number): '))
#         except Exception:
#             print('Wrong input!')
#         else:
#             if res < lower_limit or res > upper_limit:
#                 print(f'Number should be between {lower_limit} and {upper_limit}!')
#                 continue
#             else:
#                 return res
#
#
# # user_prompt = 'Enter your age'
# # number = get_int(user_prompt, 10, 30)
# # print('number is', number)
#
# number = get_int(prompt='Enter your age', lower_limit=10, upper_limit=30)
# print('number is', number)


# def get_int(prompt='Enter number', lower_limit=None, upper_limit=None):
#     while True:
#         try:
#             res = int(input(f'{prompt} (int number): '))
#         except Exception:
#             print('Wrong input!')
#         else:
#             if lower_limit is not None:
#                 if res < lower_limit:
#                     print(f'Number should be bigger than {lower_limit}!')
#                     continue
#             if upper_limit is not None:
#                 if res > upper_limit:
#                     print(f'Number should be less than {upper_limit}!')
#                     continue
#             return res
#
#
# user_prompt = 'Enter your age'
# number = get_int(user_prompt, 0)
# print('number is', number)

#
# def get_int(prompt, lower_limit=None, upper_limit=None):
#     """
#     Docstring here
#     Args:
#         prompt (str):
#         lower_limit (int):
#         upper_limit (int):
#     Returns:
#         (int):
#     """
#     while True:
#         try:
#             res = int(input(f'{prompt} (int number): '))
#         except Exception:
#             print('Wrong input!')
#         else:
#             if lower_limit is not None:
#                 if res < lower_limit:
#                     print(f'Number should be bigger than {lower_limit}!')
#                     continue
#             if upper_limit is not None:
#                 if res > upper_limit:
#                     print(f'Number should be less than {upper_limit}!')
#                     continue
#             return res


# def get_int(prompt: str, lower_limit: int = None, upper_limit: int = None) -> int:
#     while True:
#         try:
#             res = int(input(f'{prompt} (int number): '))
#         except Exception:
#             print('Wrong input!')
#         else:
#             if lower_limit is not None:
#                 if res < lower_limit:
#                     print(f'Number should be bigger than {lower_limit}!')
#                     continue
#             if upper_limit is not None:
#                 if res > upper_limit:
#                     print(f'Number should be less than {upper_limit}!')
#                     continue
#
#             return res
#
#
# res = get_int('Some number', 10, 30)


def d():
    print('In D')


def b():
    d()
    print('In B')


def c():
    print('In C')


def a():
    print('In function A')
    b()
    c()


a()

