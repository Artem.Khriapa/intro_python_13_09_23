# OOP

# class -> define structure
# object -> instance of class

# class Car:
#     brand = 'Nissan'  # class attributes
#     year = 2017
#
#
# my_car = Car()
#
# print(my_car)
# print(type(my_car))
# print(my_car.brand)  # object attributes
# print(my_car.year)
#
# my_car1 = Car()
# print(my_car1.brand)
# print(my_car1.year)
# my_car1.brand = 'Toyota'
# my_car.brand = 'BMW'
# print(my_car1.brand)
# print(my_car1.year)
#
# print(my_car.brand)
# print(my_car.year)


# class Car:
#     brand = 'Nissan'  # class attributes
#     year = 2017
#
#     def say_hello(self):  # method
#         msg = f'Hello, I\'m {self.brand} born in {self.year}'
#         return msg
#
#     def say_hello2(self, prefix):  # method
#         msg = f'{prefix}, I\'m {self.brand} born in {self.year}'
#         return msg
#
#
# my_car = Car()
#
# print(my_car.year)
# print(my_car.brand)
# print(my_car.say_hello())
# my_car.brand = 'Toyota'
# my_car.year = 2000
# print(my_car.say_hello())
# print(my_car.say_hello2('Hi'))
# print(Car.say_hello(my_car))


# class Car:
#     brand = 'Nissan'  # class attributes
#     year = 2017
#
#     def say_hello(self):  # method
#         msg = f'Hello, I\'m {self.brand} born in {self.year}'
#         return msg
#
#
# my_car = Car()
#
# print(my_car.year)
#
# my_car.color = 'Red'
#
# print(my_car.color)


# class Human:
#     name = 'Mark'
#     email = 'abcd@abc.com'
#
#
# class User(Human):
#     password = '12345678'
#
#
# h1 = Human()
# u1 = User()
#
# print(h1.email)
#
# print(u1.password)
# print(u1.email)


# class Human:
#     name = 'Mark'
#     email = 'abcd@abc.com'
#
#
# class User(Human):
#     password = '12345678'
#
#
# h1 = Human()
# u1 = User()
#
# print(type(h1))
# print(type(u1))
#
# print(isinstance(u1, User))
# print(isinstance(u1, Human))
# print(isinstance(h1, User))


# class Human:
#     name = 'Mark'
#     email = 'abcd@abc.com'
#
#
# human = Human()
#
#
# print(dir(human))
#
# attr_name = 'address'
#
# email = getattr(human, attr_name, None)
# print(email)
# print(hasattr(human, 'email'))
#
# setattr(human, attr_name, '123456')  # human.address = '123456'
# print(dir(human))
#
# delattr(human, attr_name)
# print(dir(human))


# class Human:
#     name = 'Mark'
#     email = 'abcd@abc.com'
#
#
# print(Human.__dict__)
#
# human = Human()
# print(human.__dict__)
#
# print(human.email)
# human.email = '1234567'
#
# print(human.__dict__)


# class Vehicle:
#     brand = None
#     year = None
#
#     def say_hello(self):
#         msg = f'Hello, I\'m a Vehicle'
#         return msg
#
#
# class Motorcycle(Vehicle):
#
#     def say_hello(self):
#         msg = f'Hello, I\'m motorcycle {self.brand} born in {self.year}'
#         return msg
#
#
# class Car(Vehicle):
#     seats = 4
#
#     # def say_hello(self):
#     #     msg = f'Hello, I\'m car {self.brand} born in {self.year}'
#     #     return msg
#
#
# my_car = Car()
# my_car.brand = 'Nissan'
# my_car.year = 2017
# my_bike = Motorcycle()
# my_bike.brand = 'KTM'
# my_bike.year = 2020
#
# v = Vehicle()
#
# print(v.say_hello())
# print(my_car.say_hello())
# print(my_bike.say_hello())


# class User:
#     name = None
#     age = 60
#
#     def __init__(self, new_name, new_age):
#         print('in __init__', self.name, self.age)
#         self.name = new_name
#         self.age = new_age
#
#     def say_hello(self):
#         print(f'Hello, I\'m {self.name} {self.age}')
#
#
# user1 = User('Mark', 10)
# user2 = User('Bob', 10)
#
# user1.say_hello()
# user2.say_hello()
#
# print(dir(user1))


# class A:
#     attr = 'a'
#     pass
#
#
# class B(A):
#     attr = 'b'
#     pass
#
#
# class C(B):
#     attr = 'c'
#     pass
#
#
# obj = C()
# print(obj.attr)


# MRO

class A:
    attr = 'a'
    pass


class B:
    attr = 'b'
    pass


class C(A, B):
    # attr = 'c'
    pass


obj = C()
print('---->', obj.attr)

print(C.mro())

