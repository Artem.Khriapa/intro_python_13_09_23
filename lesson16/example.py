# import threading
# import time


# def th_function(name, delay):
#     for i in range(50):
#         print(f'{name} : {i}')
#         time.sleep(delay)
#
#
# th1 = threading.Thread(target=th_function, args=('function1', 0.2))
# th2 = threading.Thread(target=th_function, args=('function2', 0.5))
#
# th1.start()
# th2.start()
#
# th1.join()
# th2.join()

# print('the end')

# counter = 0
#
# lock = threading.Lock()
#
#
# def th_function(number):
#     global counter
#
#     for i in range(1_000_000):
#         lock.acquire()
#         _counter = counter
#         lst = [i for i in range(10)]
#         _counter += 1
#         counter = _counter
#         lock.release()
#
#     print(f'thread {number}', counter)
#
#
# threads = []
#
# for i in range(5):
#     th = threading.Thread(target=th_function, args=(i,))
#     threads.append(th)
#
#
# for thread in threads:
#     thread.start()
#
# for thread in threads:
#     thread.join()
#
# print(f'counter is = {counter}')
#
# import requests
#
# urls = [
#     'https://www.google.com/search?q=python1',
#     'https://www.google.com/search?q=python2',
#     'https://www.google.com/search?q=python3',
#     'https://www.google.com/search?q=python4',
#     'https://www.google.com/search?q=python5',
#     'https://www.google.com/search?q=python6',
#     'https://www.google.com/search?q=python7',
#     'https://www.google.com/search?q=python8',
#     'https://www.google.com/search?q=python9',
#     'https://www.google.com/search?q=python10',
#     'https://www.google.com/search?q=python11',
#     'https://www.google.com/search?q=python12',
#     'https://www.google.com/search?q=python13',
#     'https://www.google.com/search?q=python14',
#     'https://www.google.com/search?q=python15',
#     'https://www.google.com/search?q=python16',
# ]
#
# begin = time.time()
# print(f'begin {begin}')
#
#
# for url in urls:
#     res = requests.get(url)
#     print(res)
#
# print(f'end {time.time() - begin}')


# def th_req(url):
#     res = requests.get(url)
#     print(res)
#
#
# threads = []
# for url in urls:
#     th = threading.Thread(target=th_req, args=(url,))
#     threads.append(th)
#
# begin = time.time()
# print(begin)
#
# for thread in threads:
#     thread.start()
#
# for thread in threads:
#     thread.join()
#
# print(f'end {time.time() - begin}')


import re

# pattern = 'bc'
# target = 'abcd'
# res = re.search(pattern, target)
#
# print(res)

# meta symbols

# () [] {} | . + ? $ ^ \

# pattern = '[a-zA-Z]'
# target = '!@#$%123D4abcd12345'
# res = re.search(pattern, target)
#
# print(res)

# pattern = '[^a-zA-Z]'
# target = 'as&dfg'
# res = re.search(pattern, target)
#
# print(res)

# pattern = '^[abc]'
# target = 'a12345abc12334'
# res = re.search(pattern, target)
#
# print(res)


# pattern = '[a-z0-9]$'
# target = 'a12345abc12334a'
# res = re.search(pattern, target)
#
# print(res)


# pattern = 'a{3}'
# target = 'a12345aaabc12334a'
# res = re.search(pattern, target)
#
# print(res)

# pattern = '[a-z]{3}[0-9]{2}'
# target = 'asd12345abc12334a'
# res = re.search(pattern, target)
#
# print(res)


# pattern = '[a-z].[0-9]'
# target = 'as12345abc12334a'
# res = re.search(pattern, target, )
#
# print(res)


# pattern = r'[a-z]{4}\.com'
# target = 'abcd.com'
# res = re.search(pattern, target, )
#
# print(res)

# pattern = r'\['
# target = 'abc[d.com'
# res = re.search(pattern, target, )
#
# print(res)

# pattern = r'[a-z]{2,4}\.[a-z]{3}'
# target = 'abcd.com'
# res = re.search(pattern, target, )
#
# print(res)


# pattern = r'[a-z.]{5}@[a-z]{3,4}\.[a-z]{3}'
# target = 'ab.cd@abcd.com'
# res = re.search(pattern, target, )
#
# print(res)

# pattern = r'[a-z]{2,5}\.{0,1}[a-z]{2,5}@[a-z]{3,4}\.[a-z]{3}'
# target = 'ab.cd@abcd.com'
# res = re.search(pattern, target, )
#
# print(res)

# pattern = r'[a-z]{2,5}\.?[a-z]{2,5}@[a-z]{3,4}\.[a-z]{3}'
# target = 'ab.cd@abcd.com'
# res = re.search(pattern, target, )
#
# print(res)


# pattern = r'[a-z]{1,}\.?[a-z]{2,5}@[a-z]{3,4}\.[a-z]{3}'
# target = 'abd.cd@abcd.com'
# res = re.search(pattern, target, )
#
# print(res)

# pattern = r'[a-z]+\.?[a-z]{2,5}@[a-z]{3,4}\.[a-z]{3}'
# target = 'a.cd@abcd.com'
# res = re.search(pattern, target, )
#
# print(res)


# pattern = r'[a-z]+\.?[a-z]{2,5}@[a-z]{3,4}\.[a-z]{3}'
# target = 'ab.cd@abcd.com'
# res = re.search(pattern, target, )
#
# print(res)
#
#
# pattern = r'[a-z]*\.?[a-z]{2,5}@[a-z]{3,4}\.[a-z]{3}'
# target = 'abcde.cd@abcd.com'
# res = re.search(pattern, target, )
#
# print(res)


# pattern = r'[abc]{3}|.{2}[0-9]'
# target = 'abde.cd123@abd.com'
# res = re.search(pattern, target, )
#
# print(res)

# pattern = r'\d{3}' # [0-9]{3}
# target = 'abde.cd123@abd.com'
# res = re.search(pattern, target, )
#
# print(res)

# pattern = r'\D{3}'  # [^0-9]{3}
# target = 'abde.cd123@abd.com'
# res = re.search(pattern, target)
#
# print(res)


pattern = r'\D{3}'  # [^0-9]{3}
regexp_compiled = re.compile(pattern)

target = 'abde.cd123@abd.com'
res = regexp_compiled.search(target)

print(res)


print('Hello git')

