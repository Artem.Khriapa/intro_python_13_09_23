"""
1. Напишіть функцію, яка визначає сезон за датою.
Функція отримує стрінг у форматі "[день].[місяць]"
(наприклад "12.01", "30.08", "1.11" і тд) і повинна
повернути стрінг з відповідним сезоном,
до якого відноситься ця дата ("літо", "осінь", "зима", "весна")
У випадку некоректних даних (35.35б 0.0) функція повинна повернути None

2. Напишіть функцію "Тупий калькулятор", яка приймає два
числових аргументи і строковий, який відповідає за
операцію між ними (+ - / *). Функція повинна повертати
значення відповідної операції (додавання, віднімання, ділення, множення),
інші операції не допускаються.
Якщо функція оримала невірний тип данних для операції (не числа)
або неприпустимий (невідомий) nтип операції вона повинна повернути
None і вивести повідомлення "Невірний тип даних" або "Операція не
підтримується" відповідно.

2*. Реалізуйте перевірку на типи данних та припустимі операції за допомогою декоратора

"""


def season(raw_str: str) -> str:
    """
    Args:
        raw_str:  `01.12`
    Returns:
        (str):
    """

    splitted = raw_str.split('.')
    if len(splitted) != 2 or not splitted[0] or not splitted[1]:
        return

    _, month = splitted

    # if month in ('12', '01', '02'):
    #     res = 'winter'
    # elif month in ('03', '04', '05'):
    #     res = 'spring'
    # elif month in ('06', '07', '08'):
    #     res = 'summer'
    # elif month in ('09', '10', '11'):
    #     res = 'autumn'

    data_dict = {
        '01': 'winter',
        '02': 'winter',
        '03': 'spring',
        '04': 'spring',
        '05': 'spring',
        '06': 'summer',
        '07': 'summer',
        '08': 'summer',
        '09': 'autumn',
        '10': 'autumn',
        '11': 'autumn',
        '12': 'winter',
    }

    return data_dict.get(month, None)


# print(season('29.13'))

#
# def stupid_calc(arg1, arg2, action):
#     """
#     Args:
#         arg1 (int | float):
#         arg2 (int | float):
#         action (str):
#     Returns:
#         (int | float | None):
#     """
#     if not isinstance(arg1, (int, float)) or not isinstance(arg2, (int, float)):
#         print('Unsupported types')
#         return None
#
#     if action == '+':
#         return arg1 + arg2
#     elif action == '-':
#         return arg1 - arg2
#     elif action == '*':
#         return arg1 * arg2
#     elif action == '/' and arg2 != 0:
#         return arg1 / arg2
#
#     print('Unsupported operation')
#
#
# print(stupid_calc(1, 0, '/'))


def stupid_calc2(arg1, arg2, action):
    """
    Args:
        arg1 (int | float):
        arg2 (int | float):
        action (str):
    Returns:
        (int | float | None):
    """

    data_actions = {
        '+': lambda a, b: a + b,
        '-': lambda a, b: a - b,
        '*': lambda a, b: a * b,
        '/': lambda a, b: a / b,
    }

    action_fn = data_actions.get(action, None)

    if action_fn:
        try:
            return action_fn(arg1, arg2)
        except Exception:
            print('Unsupported types')
    else:
        print('Unsupported operation')


print(stupid_calc2(1, 0, '*'))
