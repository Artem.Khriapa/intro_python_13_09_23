# functions

# namespace
    # global
    # local

# mutable as defaults
    # explici / implicit

# args/kwargs

#
# def slave_function1():
#     print('I\'m slave function 1')
#     return 1
#
#
# def slave_function2():
#     print('I\'m slave function 2')
#     return 2
#
#
# def base_function(arg1, arg2=0):
#     print(f'I\'m base function {arg1}, {arg2}')
#     res1 = slave_function1()
#     res2 = slave_function2()
#
#     return 10
#
#
# res = base_function(1, 2)
#
# print(res)


# global_value = 100
#
#
# def base_function():
#     global_value = 200
#     print('global_value', global_value)
#     global_value = 500
#
#     return global_value
#
#
# global_value = base_function()
#
#
# print('outside function global_value', global_value)

#
# global_value = []
#
# PI = 3.14
#
#
# def base_function(lst):
#     print('pi is :', PI)
#     print('global_value', lst)
#     lst.append(1)
#     return lst
#
#
# global_value = base_function(global_value)
#
# print('outside function global_value', global_value)

# global_value = [1, 2, 3]
#
#
# def base_function(lst):
#     for i in lst:
#         print(i)
#
#
# print('----> ')
# base_function(global_value)


# global_value = 1
#
#
# def foo():
#     print('foo', global_value)
#
#
# def base_function():
#     global global_value
#
#     print('global_value', global_value)
#     global_value += 1
#     foo()
#
#     return global_value
#
#
# print('outside function global_value', global_value)
#
# base_function()
#
# print('outside function global_value', global_value)
#
# base_function()
#
# print('outside function global_value', global_value)


# def list_mod(value, lst=[]):
#     print(id(lst))
#     lst.append(value)
#     return lst


# def list_mod(value, lst=None):
#     if lst is None:
#         lst = []
#
#     lst.append(value)
#     print(id(lst))
#     return lst
#
#
# lst1 = [1, 2, 3]


# print(f'before {lst1}')
#
# lst1 = list_mod('a', lst1)
#
# print(f'after {lst1}')


# print(list_mod('a'))
# print(list_mod('b'))
#
# lst1 = list_mod('a')
# print(lst1)
# lst2 = list_mod('b')
# print(lst2)

# # lst1 = list_mod('v', lst1)
# #
# # print(f'after {lst1}')
#
# lst = list_mod('c')
# print(lst)


# def my_function(arg1, arg2, arg3=3, arg4=4):
#     print(f'arg1 {arg1}, arg2 {arg2}, arg3 {arg3}, arg4 {arg4}')
#
#
# my_function(1, 2)


# def my_function(*args):
#     print(type(args))
#     print(f'args {args}')
#
#
# my_function(1, 2, 3)
# my_function(1)
# my_function(1, 2, 3, 3, 4, 3, 3, 3, 3, 3, 3, 3, 33, 3)


# def my_function(arg1, arg2, *args):
#     print(f'arg1 {arg1}, arg2 {arg2}')
#     print(f'args {args}')
#
#
# my_function(1, 2)
# my_function(1, 2, 3, 4, 5, 6)
#
# lst = [1, 2, 3, 4, 5, 6]
#
# my_function(*lst)  # my_function(1, 2, 3, 4, 5, 6)


# def foo(a, b, *args):
#     print(a, b)
#
#
# lst = [1, 2, 3, 4]
#
# foo(*lst)

# lst = [1, 2, 3, 4]
#
# a, b, *c = lst
# print(a, b, c)
#
# *a, b = lst
# print(a, b)
#
# # lst = [1]
# #
# # a, *b, c = lst
# # print(a, b, c)
#
#
# lst = [1, 2]
# a, b = lst
# print(a, b)


# def foo(a, b=1, *args):
#     print(a, b)
#     for arg in args:
#         print('-->', arg)
#
#
# lst = [1, 2, 3, 4]
#
# foo(*lst)


# def foo(**kwargs):
#     print(type(kwargs))
#     print(kwargs)
#     # for argname, value in kwargs.items():
#     #     print('-->', argname, value)
#
#
# foo(a=1, b=2, c=3)
#
# dct = {
#     'a': 1,
#     'b': 2,
#     'c': 3,
# }
#
# foo(**dct)  # foo(a=1, b=2, c=3)


# def foo(a, b, c=0, **kwargs):
#     print(a, b, c)
#     print(kwargs)
#     # for argname, value in kwargs.items():
#     #     print('-->', argname, value)
#
#
# dct = {
#     # 'a': 1,
#     # 'b': 2,
#     'c': 3,
#     'd': 4,
# }
#
# lst = [1, 2]
#
# foo(*lst, **dct)


# def foo(a, b, c=0, d=0, e=0, *args, **kwargs):
#     print(a, b, c, d, e)
#     print(args)
#     print(kwargs)
#
#
# dct = {
#     # 'a': 1,
#     # 'b': 2,
#     'c2': 3,
#     'd2': 4,
#     'e': 5,
# }
#
# lst = [1, 2]
#
# foo(1, *lst, d=4, **dct)


# guess the number

from random import randint


def get_user_number(prompt='Enter number', lower_limit=None, upper_limit=None):
    while True:
        try:
            res = int(input(f'{prompt} (int number): '))
        except Exception:
            print('Wrong input!')
        else:
            if lower_limit is not None:
                if res < lower_limit:
                    print(f'Number should be bigger than {lower_limit}!')
                    continue
            if upper_limit is not None:
                if res > upper_limit:
                    print(f'Number should be less than {upper_limit}!')
                    continue
            return res


def get_comp_number(lower_limit, upper_limit):
    res = randint(lower_limit, upper_limit)
    print(f'Hint: {res}')
    return res


def compare_numbers(comp_number, user_number):
    if comp_number == user_number:
        print('!')
        return True
    elif abs(comp_number - user_number) < 3:
        print('hot')
        return False



def game_guess_number():
    lower_limit = 0
    upper_limit = 9

    comp_number = get_comp_number(lower_limit, upper_limit)

    while True:

        user_number = get_user_number(f'Guess the number [{lower_limit}-{upper_limit}]', lower_limit, upper_limit)

        if user_number == comp_number:
            print('Congratulations!')
            break
        else:
            print('Try again!')

        # is_guessed = compare_numbers(comp_number, user_number)


game_guess_number()


