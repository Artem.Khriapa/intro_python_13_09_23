# imports
#   modules
#   relative

# import time
#
# print(time.time())

# import time
# import random
#
# print(time.time())
# print(random.randint(1, 10))


# from random import randint, random
#
# print(randint(1, 10))
# print(random())

# import random as RANDOM
# import time as Time
#
# print(RANDOM.randint(1, 10))
# print(Time.time())


# from random import randint as random_int
# print(random_int(1, 10))

# print(dir())
# from random import *
# print(randint(1, 10))


# import requests
# res = requests.request('GET', 'https://docs.python.org/')
# print(res)


# import library
#
# print(dir(library))
# print(library.var1)
# print(library.var2)
# library.foo()
# library.bar()


# from library import foo as foo_function
#
# foo_function()

# print('before import')
# import library  # python3 library.py
# print('after import')
#
# library.foo()

# print('before import')
# from library import foo  # python3 library.py
# print('after import')
#
# foo()


# print('__name__ in example:', __name__)
#
# from library import foo  # python3 library.py
#
# foo()


# from ex1 import a
# print(a.vara)

# import ex1
# print(ex1.vara)


# useful functions
# data transformation (all types)

# var = bool([1, 2])
# print(var)
#
# my_int = int(1234.3)
# print(my_int)
#
# var = float(True)
# print(var)
#
# var = str(True)
# print(var)
#
# var = list('range(1, 10)')
# print(var)
#
# var = tuple('range(1, 10)')
# print(var)
#
# var = set('range(1, 10)')
# print(var)
#
# var = dict(a=10, b=20)
# print(var)
#
# var = dict([('a', 10), ('b', 20)])
# print(var)


# math (sum, min, max, abs)
#
# res = sum([1, 2, 3, 5])
# print(res)
#
# res = sum([1, 2, 3, 5], start=100)
# print(res)

# lst = [10, 12, 4, 7, 1, -2, 3, 5, 33]
#
# res = min(1, 2, 3, 4, 5, 0)
# print(res)
#
#
# def foo(v):
#     return v % 3
#
#
# res = min(lst, key=foo)
# print(res)
#
#
# res = max(lst, key=foo)
# print(res)
#
#
# res = max(lst)
# print(res)
#
# res = abs(1)
# print(res)


# iterators (range, enumerate, map, zip, reversed, filter)

# range_obj = range(10, 1, -2)
#
# for i in range_obj:
#     print(i)


# my_str = '0123456789'

# for i in range(len(my_str)):
#     print(i, my_str[i])
#
# my_str = set('0123456789')
#
# enumerated = enumerate(my_str)
#
# for i in enumerated:
#     print(i)


# def foo(v):
#     return v ** 2
#
#
# # for i in range(10):
# #     print(foo(i))
#
#
# mapped = map(foo, range(10))
# for i in mapped:
#     # i = foo(i)
#     print(i)

#
# lst1 = [1, 2, 3, 4, 5, 6]
# tpl1 = ('a', 'b', 'c', 'd', 'e', 'f')
# set1 = {'a', 'b', 'c'}
#
# # for i in range(6):
# #     print(lst1[i], tpl1[i])
#
# zipped = zip(lst1, tpl1, set1)
#
# for i in zipped:
#     print(i)


# lst1 = [1, 2, 3, 4, 5, 6]

# reversed_obj = reversed(lst1)
#
# for i in reversed_obj:
#     print(i)


# lst1 = [1, 2, 3, 4, 5, 6]


# def foo(v):
#     return v % 3 == 0
#
#
# filtered_obj = filter(foo, lst1)
#
# for i in filtered_obj:
#     print(i)


# lst1 = [1, 2, 3, 4, 5, 6]
#
# filtered_obj = filter(lambda v: v % 3 == 0, lst1)
#
# for i in filtered_obj:
#     print(i)


# lst1 = [11, -2, 3, 14, 5, -6]
#
#
# def foo(v):
#     return v % 3
#
#
# sorted_obj = sorted(lst1, reverse=True, key=foo)
#
# print(sorted_obj)


#   conditions(any, all)

# a = 5
# b = 10
#
#
# conditions = (
#     a > 0,
#     b > 0,
#     b > a,
# )
#
# if all(conditions):
#     print('match!')
#
#
# if any(conditions):
#     print('match!')


# lambda functions

# lst1 = [1, 2, 3, 4, 5, 6]

#
# def foo(v):
#     return v % 3 == 0
#
#
# filtered_obj = filter(lambda v: v % 3 == 0, lst1)
#
# for i in filtered_obj:
#     print(i)

#
# lst1 = [1, 2, 3, 4, 5, 6]
#
# foo = lambda v: v % 3 == 0  # bad idea
#
# filtered_obj = filter(foo, lst1)


# lst1 = ['1', '2', '3', '4', '5', '6']
#
# filtered_obj = list(filter(lambda v: int(v), lst1))  # bad idea
# print(filtered_obj)
#
# filtered_obj = list(filter(int, lst1))  # bad idea
# print(filtered_obj)
#
# filtered_obj = list(filter(lambda v: int(v) % 2 == 0, lst1))
# print(filtered_obj)


# lambda *args, **kwargs: smth with *args, **kwargs

calc = {
    '+': lambda a, b: a + b,
    '-': lambda a, b: a - b,
    '*': lambda a, b: a * b,
    '/': lambda a, b: a / b,
}

print(calc['+'](2, 3))

