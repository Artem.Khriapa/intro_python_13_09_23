# my_list = []
# for i in range(11):
#     my_list.append(i)
#
#
# my_dict = {i: str(i**2) for i in range(11) if i % 2 == 0}


import requests


# response = requests.request('GET', 'https://www.python.org/')

# with open('python.html', 'w') as file:
#     file.write(response.text)

from requests import get
from pprint import pprint

# try:
#     response = get('https://www.python.org/')
# except:
#     print('Error')
# else:
#     if 200 <= response.status_code < 300:
#         if 'text/html' in response.headers.get('Content-Type', ''):
#             with open('python2.html', 'w') as file:
#                 file.write(response.text)

# img_url = 'https://requests.readthedocs.io/en/latest/_static/requests-sidebar.png'
#
# try:
#     response = get(img_url)
# except:
#     print('Error')
# else:
#     if 200 <= response.status_code < 300:
#         print(response.headers)
#         if 'image/png' in response.headers.get('Content-Type', ''):
#             with open('requests.png', 'xb') as file:
#                 file.write(response.content)


url = "https://api.kuna.io/v4/order/public/book/BTC_USDT"

headers = {
    "accept": "application/json",
}

params = {
    'level': 5
}
try:
    response = get(url, headers=headers, params=params)
except:
    print('Error')
else:
    if 200 <= response.status_code < 300:
        print(response.headers)
        if 'application/json' in response.headers.get('Content-Type', ''):
            pprint(response.content)
            data = response.json()
            pprint(data)
            pprint(data.get('data', {}).get('asks', []))

