from flask import Flask, render_template

app = Flask(__name__)


@app.route('/index')
def index():
    return 'hello world'


@app.route('/template')
def templates():
    data = ['hello', 'world', 'foo', 'bar', 'baz', 'ololo']
    return render_template('template1.html', context=data)



