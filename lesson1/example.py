# a = 10
# b = 20
# c = a + b
#
# print(c)
#
# a = 10
# A1234 = 20
#
# students_counter = 8  # comment

# l = 0
# I = 0
# o = 0
# O = 0

# numbers

# # int
#
# my_integer = 100
# my_integer2 = 100
# print(my_integer)
#
# my_integer = my_integer + 1
# print(my_integer)
#
# my_integer = my_integer - my_integer2
# print(my_integer)
#
# my_integer = my_integer * 3
# print(my_integer)
#
# result = my_integer / my_integer2
# print(result)
#
# my_integer = my_integer + my_integer2 + my_integer2
# print(my_integer)
#
# # my_integer = my_integer / 0
# # print(my_integer)
#
# my_integer = 20 // 3
# print(my_integer)
#
# my_integer = 20 % 3
# print(my_integer)
#
# my_integer = 2 ** 3
# print(my_integer)
#
# print(type(my_integer))
# print(type(10 / 5))


# float
#
# my_float = 1.2
# print(my_float)
#
# my_float = my_float + 3.4
# print(my_float)
#
# my_float = 3 + 3.0
# print(my_float)
#
# my_float = 1.5e-4  # 1.5 * (10 ** -4)
# print(my_float)
#
# my_float = 0.0010000000000001
# print(my_float)
#
# my_float = 0.1 + 0.2
# print(my_float)
#
# my_float += 10  # my_float = my_float + 10
# print(my_float)
#
# my_float *= 10
# print(my_float)
#
# my_float = (my_float + 3) ** 4.99
#
# print(my_float)
# my_int = int(my_float)
# print(my_int)
#
# my_float = float(123)
# print(my_float)


# bool

# my_bool = True
# my_bool = False
#
# my_int1 = 120
# my_int2 = 20
#
# my_bool = my_int1 > my_int2
# print(my_bool)
#
# my_bool = my_int1 < my_int2
# print(my_bool)
#
# my_bool = my_int1 >= my_int2
# print(my_bool)
#
# my_bool = my_int1 <= my_int2
# print(my_bool)
#
# my_bool = my_int1 == my_int2
# print(my_bool)
#
# my_bool = my_int1 != my_int2
# print(my_bool)
#
# # my_bool = 0.3 == 0.1 + 0.2
# # print(my_bool)
#
# print(type(my_bool))

# result = my_bool + 1
# print(result)

# result = int(True)
# print(result)
#
# result = int(False)
# print(result)
#
# result = bool(-0.00000000000000000000090)
# print(result)
#
# result = bool(0)
# print(result)
#
# # None
#
# my_none = None
#
# result = bool(None)
# print(type(None))
# print(result)


# str

# my_str = '1234567'
# print(type(my_str))
#
# my_str = "1234567"
# print(type(my_str))
#
# my_str = "1"
# print(my_str)
#
# my_str = ""
# print(type(my_str))

# my_str = '1234567'
# print(my_str)
#
# result = my_str + 'abcdef'
# print(result)
#
# result = my_str * 3
# print(result)
#
#
# my_str = '123\n4567'
# print(my_str)
#
# my_str = '''123456
# 123456
# 123456'''
# print(my_str)
#

# '''
# multiline comment
# multiline comment
# multiline comment
# multiline comment
# '''

# my_str = """123456
# 123456
# 123456"""
# print(my_str)

# my_str = '123"456'
# print(my_str)
# my_str = '123"\'456'
# print(my_str)
#
# my_value = 12345
# print(type(my_value))
#
# my_value = str(my_value)
# print(type(my_value))
#
# my_int = int('12345')
# print(my_int)
#
# my_float = float('123.45')
# print(my_float)
#
# my_bool = bool('\n')
# print(my_bool)
#
# val1 = str(1234)
# my_value = 'abcd' + '1234' + val1
# print(my_value)


name = "Artem"
age = 40

# My name is Artem, I'm 40 years old

message = "My name is " + name + ", I'm " + str(age) + " years old"
print(message)

message = "My name is %s, I'm %s years old" % (name, age)
print(message)

template = "My name is %s, I'm %s years old"
message = template % (name, age)
print(message)

template = "My name is {}, I'm {} years old"
message = template.format(name, age)
print(message)

template = "My name is {}, I'm {} years old"
message = template.format("Bill", 65)
print(message)

template = "My name is {name_place}, I'm {age_place} years old"
message = template.format(age_place=age, name_place=name)
print(message)

template = "My name is {name_place}, I'm {age_place} years old".format(age_place=age, name_place=name)
print(message)

message = f"My name is {name}, I'm {age} years old"
print(message)
