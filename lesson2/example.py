#

# my_str = 'abcdef'
#
# print('b' in my_str)
# print(len(my_str))
#
# my_str = '1234.56'
# print(my_str.isdigit())
#
# my_str = 'asdfghjk'
# print(my_str.isalpha())
#
# my_str = 'qwdwef-12345'
# print(my_str.isalnum())
#
# my_str = 'abc123abc'
# print(my_str.startswith('a'))
#
# my_str = 'abc123abc'
# print(my_str.endswith('bc'))
#
# result = my_str.endswith('bc')
# print(type(result))
#
# my_str = '   abc--123-abc---'
# result = my_str.strip()
# print(result)
#
# my_str = '--abc--123-abc---'
# result = my_str.lstrip('-')
# print(result)
#
# my_str = '--abc--123-abc---'
# result = my_str.rstrip('-')
# print(result)
#
#
# my_str = '--abc--123-abc---'
# result = my_str.replace('-', '*')
# print(result)
#
# my_str = '--abc--123-abc---'
# result = my_str.upper()
# print(result)
#
# my_str = '--ABCbc--123-aJGFbc---'
# result = my_str.lower()
# print(result)
#
# my_str = 'ABCbc JGFbc'
# result = my_str.title()
# print(result)
#
# my_str = 'ABCbc JGFbc'
# result = my_str.capitalize()
# print(result)
#
# my_str = 'ABCbc JGFbc'
# result = my_str.count('bc')
# print(result)

# # my_str = '0123456789'
# # result = my_str.index('0')
# my_str = 'abcdefghij'
# result = my_str.index('cd')
# print(result)
#
# my_str = 'abcdefghij'
# result = my_str.find('f')
# print(result)

# my_str = 'abcdefghij'
# result = my_str[3]
# print(result)


# my_str = '0123456789'
# last_idx = len(my_str) - 1
#
# result = my_str[last_idx]
# print(result)
#
# my_str = '0123456789'
# last_idx = len(my_str) - 1
#
# result = my_str[last_idx // 2]
# print(result)

# my_str = '0123456789'
# result = my_str[-4]  # my_str[len(my_str) - 1]
# print(result)

# my_str = '0123456789'
# print(my_str)
# result = my_str[2:5]
# print('my_str[2:5] -> ', result)
#
# result = my_str[0:5]
# print('my_str[0:5] -> ', result)
#
# result = my_str[:5]  # my_str[0:5]
# print('my_str[:5] -> ', result)
#
# result = my_str[5:10]
# print('my_str[5:10] -> ', result)
#
# result = my_str[5:]
# print('my_str[5:] -> ', result)
#
# result = my_str[:]
# print('my_str[:] -> ', result)
#
# result = my_str[-6:-1]
# print('my_str[-6:-1] -> ', result)
#
# result = my_str[1:8:3]
# print('my_str[1:8:3] -> ', result)
#
# result = my_str[8:1:-1]
# print('my_str[8:1:-1] -> ', result)
#
# result = my_str[::-1]
# print('my_str[::-1] -> ', result)
#
# aver_idx = (len(my_str) // 2) - 1
# result = my_str[aver_idx::2]
# print('my_str[len(my_str)//2::2] -> ', result)

# if/else

# first = 20
# second = 20
#
# result = first == second
#
# if result:
#     print('equal')
#     print('equal')
#     print('equal')
# else:
#     print('not equal')
#     print('not equal')
#     print('not equal')

# first = 20
# second = 10
#
# if first == second:
#     print('equal')
# else:
#     print('not equal')

# first = 20
# second = 30
#
# if first > second:
#     print('first > second')
# elif first < second:
#     print('first < second')
# else:
#     print('first = second')


# first = 20
# second = 20
#
# if first > second:
#     print('first > second')
# elif first < second:
#     print('first < second')
# elif first < 0:
#     print('first < 0 ')
# else:
#     print('first == second')

# first = -20
# second = -30
#
# if first > second:
#     print('first > second')
#
#     if first < 0:
#         print('first < 0 ')
#
# elif first < second:
#     print('first < second')
# else:
#     print('first == second')

# userinput = input('Enter something: ')
#
#
# if userinput:  # bool(userinput)
#     print(userinput)
# else:
#     print('Empty')


# userinput = int(input('Enter something: '))
#
#
# if userinput:  # bool(userinput)
#     print(userinput)
# else:
#     print('zero')


# userinput = input('Enter something: ')
#
# if userinput.isdigit():
#     number = int(userinput)
#     if number:
#         print(userinput)
#     else:
#         print('It\'s zero')
# else:
#     print('It\'s not a number')


# first = 20
# second = 30
#
# if first > second or first < second or first < 0:
#     print('first > second or first < second or first < 0')
# else:
#     print('first = second')


# res = 0 or '' or False or 0.0
#
# print(res)


# first = 20
# second = 30
#
# if first > second and first > 0 and second > 0:
#     print('first > second and first > 0 and second > 0')
# else:
#     print('Not match')

# res = 0 and '234' and 2.3
# print(res)
#
#
# first = 20
# second = 30
#
# if first > second or (first > 0 and second > 0):
#     print('first > second or first > 0 and second > 0')
# else:
#     print('Not match')

first = -20
second = -30

first_condition = first > second
second_condition = first > 0 and second > 0

if first_condition or not second_condition:
    print('first > second or first > 0 and second > 0')
else:
    print('Not match')

