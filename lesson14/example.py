# # iterator
#
# data = 'Hello world!'
#
# iterator = iter(data)
#
# print(iterator)
# print(next(iterator))
#
# for i in data:  # iterator = iter(data)
#     # try:
#     #     i = next(iterator)
#     # except StopIteration:
#     #     break
#     print(i)
#
#
# # iterator = iter(data)
# # while True:
# #     try:
# #         i = next(iterator)
# #     except StopIteration:
# #         break

# class MyIter:
#
#     limit = 0
#     current = 0
#
#     def __init__(self, limit):
#         self.limit = limit
#
#     def __iter__(self):
#         self.current = 0
#         return self
#
#     def __next__(self):
#         self.current += 1
#         if self.current > self.limit:
#             raise StopIteration
#
#         return self.current - 1
#
#
# my_obj = MyIter(5)
#
# for i in my_obj:
#     print(i)
#
# for i in my_obj:
#     print(i)


# file = open('my_file.txt', 'a')
# # for line in file:
# #     print(line)
# file.write('string from python2')
# file.close()

#
# with open('my_file.txt', 'a') as file:  # file = open('my_file.txt', 'a')
#     file.write('\nHello World')
#     file.write('\nHello World4')
#     file.write('\nHello World5')
#     file.write('\nHello World6')
#     # file.close()


# with open('my_new_file.txt', 'x') as file:  # file = open('my_file.txt', 'a')
#     file.write('\nHello World')
#     file.write('\nHello World4')
#     file.write('\nHello World5')
#     file.write('\nHello World6')
#     # file.close()
#
# import time
#
#
# class MyClass:
#
#     def __enter__(self):
#         self.begin = time.time()
#         return self
#
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         print(time.time() - self.begin)
#         print(exc_type)
#         print(exc_val)
#         print(exc_tb)
#
#         return True
#
#
# obj = MyClass()
#
# with obj as context_obj:
#     print('inside with1')
#     print('inside with2')
#     lst = [i for i in range(1000000)]
#
# print('outside with')


# class MyClass:
#
#     attr1 = 0
#     attr2 = 10
#
#     def __init__(self, attr1_value):
#         print('__init__')
#         self.attr1 = attr1_value
#         self.attr2 = input('Enter attr 2 for object: ')
#
#
# obj1 = MyClass(100)
# obj2 = MyClass(200)
#
# print(obj1.attr1)
# print(obj1.attr2)
#
# print(obj2.attr1)
# print(obj2.attr2)
#
# obj1.attr1 = 1234
# obj2.attr2 = 4321
#
# print(obj1.attr1)
# print(obj2.attr2)


class Singleton:

    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is not None:
            return cls._instance

        cls._instance = object.__new__(cls)
        return cls._instance


o1 = Singleton()
print(id(o1))

o2 = Singleton()
print(id(o2))

o3 = Singleton()
print(id(o2))


