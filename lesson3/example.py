# a = 10
# b = 0
#
# while a > b:
#     print(a, b)
#     b += 1
#
#
# print('The end')

# a = 10
# b = 0
#
# while True:
#     if a < b:
#         break
#
#     print(a, b)
#     b += 1


# a = 10
# b = 0
#
# while True:
#     b += 1
#
#     if b % 2 == 0:
#         print(a, b)
#     else:
#         continue
#
#     if a < b:
#         break

# counter = 0
#
# while counter < 5:
#     print('counter', counter)
#     inner_counter = 0
#     while True:
#         print('----> inner_counter', inner_counter)
#         inner_counter += 1
#
#         if inner_counter >= 3:
#             break
#
#     counter += 1


# value = input('Enter some number:')
#
# while not value.isdigit():
#     print('Wrong!')
#     value = input('Enter some number:')
#
# print('THNX')


# while True:
#     value = input('Enter some number:')
#
#     if value.isdigit():
#         print('THNX')
#         break
#
#     print('Wrong!')


# while True:
#     value = input('Enter some number:')
#
#     if not value.isdigit():
#         print('Wrong!')
#         continue
#
#     print('THNX')
#     break

# value = ''
#
# while not value.isdigit():
#     value = input('Enter some number:')

# my_str = 'abcd adf wetr wf srg dfv'
# print(my_str)
#
# res = my_str.split(' ')
# print(res)
#
# print(type(res))
#
# my_list = [1, 2.2, True, 'F', [], None]
# my_list.append(1)
# print(my_list)

# my_list = []
# my_list.append('asdfg')
# my_list.append(True)
# my_list.append(False)
# my_list.append([1, 2, 3])
# my_list.append(False)
#
# print(my_list)
# my_list.remove(123)
#
# print(my_list)

#
# my_list = [1, 2.2, True, 'F', [], None]
#
# print(1 in my_list)
# print(len(my_list))
#
# print(my_list[3])
# print(my_list[1:3])
#
# print(my_list)
# my_list[3] = 'poiuytre'
# print(my_list)
# print(my_list[-1])

#
# my_list = [1, 2.2, True, 'Four', [], None, 1]
# print(my_list[3])
# print(type(my_list[3]))
# print(my_list[3].isdigit())
#
# my_list = [1, 2.2, True, 'Four', [1, 2, 3], None]
# print(my_list[4][-1])
#
# matrix = [[1, 2, 3], [4, ['a', 'b', 'c'], 6], [7, 8, 9]]
# print(matrix[1][1][-1])
#
# matrix.pop(2)
# print(matrix)
#
# print(bool([1, 2]))
# print(bool([]))
#
#
# matrix += [1, 2, 3]
#
# print(matrix)
#
# matrix.extend([1, 2, 3])
# print(matrix)
#
# matrix = matrix * 2
# print(matrix)

# print([1, 2, 3] == [2, 3, 1])
#
# lst = list('123456')
# print(lst)

# # tuple
#
# my_tuple = (1, 2, 3, 4)
#
# print(my_tuple[1:2])
#
#
# lst = list(my_tuple)
# print(lst)
#
# tpl = tuple('lst')
# print(tpl)


# lst = [1, 2.2, True, 'Four', None]
#
# limit = len(lst)
# idx = 0
# while idx < limit:
#     print(lst[idx])
#     idx += 1

# lst = [1, 2.2, True, 'Four', None]
#
# for list_item in lst:
#     print(list_item, type(list_item))

#
# tpl = (1, 2.2, True, 'Four', None)
#
# for tpl_item in tpl:
#     print(tpl_item, type(tpl_item))


# for str_item in '1234567890':
#     if int(str_item) % 2 == 0:
#         continue
#     if str_item == '5':
#         break
#     print(str_item, type(str_item))

# for item in range(3):
#     print(item)


# for _ in range(3):
#     user_input = input('Enter number: ')
#     if user_input.isdigit():
#         user_input = int(user_input)
#         break
#     else:
#         print('Try again')
#
# if type(user_input) == int:
#     print('It\'s a number')
# else:
#     print('It\'s not a number')


# try / except
# while True:
#     try:
#         user_age = 10 / float(input('Enter ege: '))
#     except ValueError as err:
#         print('Not a number')
#         print(err)
#         continue
#     except ZeroDivisionError as err:
#         print('Not a zero')
#         continue
#     else:
#         break


# while True:
#     try:
#         user_age = 10 / float(input('Enter ege: '))
#     except (ValueError, ZeroDivisionError):
#         print('Not a number or zero')
#         continue
#     else:
#         break
#     finally:
#         print('finally')


while True:
    try:
        user_age = 10 / float(input('Enter ege: '))
    except (ValueError, ZeroDivisionError):
        print('Not a number or zero')
        continue

    break
