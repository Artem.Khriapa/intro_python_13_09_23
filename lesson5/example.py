"""
1. напишіть програму, яка при заданому цілому n повертає
n + nn + nnn
5 -> 5 + 55 + 555
13 -> 13 + 1313 + 131313


2. Напишіть програму, яка відповідає на питання,
чи введений стрінг є паліндромом

abcdcba -> True
aabbcc -> False


3. AI порграма має відповісти на питання чи є введений стрінг
1 - номер телефону
2 - email
3 - Name

+380631112233 -> Phone
bcdef@abc.efg -> email   3+ letters @ 3 letters. 3 letters
Bill J.I. -> name   2 words
something else -> unknown

"""


# 1. напишіть програму, яка при заданому цілому n повертає
# n + nn + nnn
# 5 -> 5 + 55 + 555
# 13 -> 13 + 1313 + 131313

# n = 13
# res = n + int(str(n) * 2) + int(str(n) * 3)
# print(res)

# 2. Напишіть програму, яка відповідає на питання,
# чи введений стрінг є паліндромом
#
# abcdcba -> True
# aabbcc -> False

# raw_str = 'qwerabc'
#
# if raw_str == raw_str[::-1]:
#     print('It\'s palindrome')
# else:
#     print('It\'s not a palindrome')

#
# raw_str = 'qwsrewq'
#
# f_idx = 0
# r_idx = -1
#
# awg_idx = len(raw_str) // 2
#
# while True:
#     if raw_str[f_idx] != raw_str[r_idx]:
#         print(False)
#         break
#     f_idx += 1
#     r_idx -= 1
#     if f_idx > awg_idx:
#         break
# else:
#     print(True)

#
# raw_str = 'Bill J.I.'
#
# result = 'Unknown'
#
# if len(raw_str) == 13 and raw_str[0] == '+':
#     if raw_str[1:].isdigit() and raw_str[3:6] in ('063', '067', '050'):
#         result = 'Phone number'
# elif len((splited := raw_str.split(' '))) == 2:
#     # Bill J.I.
#     _name = splited[0]
#     _initials = splited[1]
#     if _name.isalpha() and _name.capitalize() == _name:
#         if _initials[-1] == '.' and _initials[-3] == '.' and _initials[0].isalpha() and _initials[0].upper() == _initials[0] and _initials[2].isalpha() and _initials[2].upper() == _initials[2]:
#             result = 'Name'
# # elif :
# #     print('Email')
#
# print(result)


raw_str = ''

result = 'Unknown'

phone_conditions = (
    len(raw_str) == 13,
    raw_str[0] == '+',
    raw_str[1:].isdigit(),
    raw_str[3:6] in ('063', '067', '050'),
)

if all(phone_conditions):
    result = 'Phone number'
elif len((splited:= raw_str.split(' '))) == 2:
    # Bill J.I.
    _name = splited[0]
    _initials = splited[1]
    if _name.isalpha() and _name.capitalize() == _name:
        if _initials[-1] == '.' and _initials[-3] == '.' and _initials[0].isalpha() and _initials[0].upper() == _initials[0] and _initials[2].isalpha() and _initials[2].upper() == _initials[2]:
            result = 'Name'
# elif :
#     print('Email')

print(result)

